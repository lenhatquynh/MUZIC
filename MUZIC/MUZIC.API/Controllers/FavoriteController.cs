﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using MUZIC.Model;

namespace MUZIC.API.Controllers
{
    public class FavoriteController : ConBase
    {
        [HttpGet("get/list/song/{username}")]
        public async Task<ActionResult<object>> GetSongs(string username)
        {
            var res = await Program.Sql.QueryAsync<Song>(@"
                with with_row_num as (
                  select 
                    * ,
                    ROW_NUMBER() over(partition by Username, SongId order by Date desc) as row_num
                  from SongTracking
                  where (EventType = 2 or EventType = 3) and Username=@Username
                ), user_favo as (
                  select SongId from with_row_num where row_num = 1  and EventType=2
                )
                select * from song where Id in (select * from user_favo)
            ", new
            {
                Username = username
            });

            return new
            {
                status = 0,
                data = res
            };
        }

        [HttpGet("get/list/playlist/{username}")]
        public async Task<ActionResult<object>> GetPlaylists(string username)
        {
            var res = await Program.Sql.QueryAsync<Playlist>(@"
                with with_row_num as (
                  select 
                    * ,
                    ROW_NUMBER() over(partition by Username, PlaylistId order by Date desc) as row_num
                  from PlaylistTracking
                  where (EventType = 2 or EventType = 3) and Username=@Username
                ), user_favo as (
                  select PlaylistId from with_row_num where row_num = 1  and EventType=2
                )
                select * from Playlist where Id in (select * from user_favo)
            ", new
            {
                Username = username
            });

            return new
            {
                status = 0,
                data = res
            };
        }

        [HttpGet("check/song/{username}/{songId:int}")]
        public async Task<ActionResult<object>> IsSongFavorited(string username, int songId)
        {
            var res = await Program.Sql.ExecuteScalarAsync<int>(@"
                select top 1 EventType
                from SongTracking
                where Username=@Username and SongId=@SongId and (EventType=2 or EventType=3)
                order by [Date] desc
            ", new
            {
                Username = username,
                SongId = songId
            });

            return new
            {
                status = 0,
                data = res == 2
            };
        }

        [HttpGet("check/playlist/{username}/{playlistId:int}")]
        public async Task<ActionResult<object>> IsPlaylistFavorited(string username, int playlistId)
        {
            var res = await Program.Sql.ExecuteScalarAsync<int>(@"
                select top 1 EventType
                from PlaylistTracking
                where Username=@Username and PlaylistId=@PlaylistId and (EventType=2 or EventType=3)
                order by [Date] desc
            ", new
            {
                Username = username,
                PlaylistId = playlistId
            });

            return new
            {
                status = 0,
                data = res == 2
            };
        }

        [HttpPost("enable/song")]
        public async Task<ActionResult<object>> OnSong([FromForm] string username, [FromForm] int songId)
        {
            await Program.Sql.ExecuteAsync(Extension.GetInsertQuery("SongTracking", "Username", "SongId", "EventType"), new
            {
                Username = username,
                SongId = songId,
                EventType = 2
            });

            return new
            {
                status = 0
            };
        }

        [HttpPost("disable/song")]
        public async Task<ActionResult<object>> OffSong([FromForm] string username, [FromForm] int songId)
        {
            await Program.Sql.ExecuteAsync(Extension.GetInsertQuery("SongTracking", "Username", "SongId", "EventType"), new
            {
                Username = username,
                SongId = songId,
                EventType = 3
            });

            return new
            {
                status = 0
            };
        }

        [HttpPost("enable/playlist")]
        public async Task<ActionResult<object>> OnPlaylist([FromForm] string username, [FromForm] int playlistId)
        {
            await Program.Sql.ExecuteAsync(Extension.GetInsertQuery("PlaylistTracking", "Username", "PlaylistId", "EventType"), new
            {
                Username = username,
                PlaylistId = playlistId,
                EventType = 2
            });

            return new
            {
                status = 0
            };
        }

        [HttpPost("disable/playlist")]
        public async Task<ActionResult<object>> OffPlaylist([FromForm] string username, [FromForm] int playlistId)
        {
            await Program.Sql.ExecuteAsync(Extension.GetInsertQuery("PlaylistTracking", "Username", "PlaylistId", "EventType"), new
            {
                Username = username,
                PlaylistId = playlistId,
                EventType = 3
            });

            return new
            {
                status = 0
            };
        }
    }
}
