﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using MUZIC.Model;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;

namespace MUZIC.API.Controllers
{
    public class PlaylistController : ConBase
    {
        [HttpGet("get/duration/{id}")]
        public async Task<ActionResult<object>> GetDuration(int id)
        {
            var res = await Program.Sql.ExecuteScalarAsync<int>(@"
                select sum(Duration) from Song
                right join PlaylistSong on Song.Id = PlaylistSong.SongId
                where PlaylistSong.PlaylistId = @id"
            , new { id });
            return new
            {
                status = 0,
                data = res
            };
        }

        [HttpGet("get/info/{playlistId:int}")]
        public async Task<ActionResult<object>> get(int playlistId)
        {
            var li = (await Program.Sql.QueryAsync<Playlist>("select * from Playlist where Id=@Id", new Playlist
            {
                Id = playlistId
            })).AsList();

            if (li.Count == 0) return new
            {
                Status = -1,
                Message = $@"Playlist ""{playlistId}"" not found"
            };

            return new
            {
                Status = 0,
                Data = li.First()
            };
        }



        //Author: Nguyen Khanh Duy
        [HttpGet("get/top/{n:int}")]
        public async Task<ActionResult<object>> GetTop(int n)
        {
            var li = (await Program.Sql.QueryAsync<Playlist?>("select top (" + n + ") * from Playlist Order by Date desc")).AsList();

            if (li.Count == 0) return new
            {
                Status = -1,
                Message = $@"Playlist ""{n}"" is empty"
            };

            return new
            {
                Status = 0,
                Data = li
            };
        }

        //Search 
        //Author: Nguyen Khanh Duy
        [HttpGet("search/{title}")]
        public async Task<ActionResult<object>> GetTitle(string title)
        {
            var li = (await Program.Sql.QueryAsync<Playlist>("Select * from Playlist where IsPublic = 'True' and dbo.rmvAccent(Title) like concat(N'%',dbo.rmvAccent(@Word),'%')", new
            {
                Word = title
            })).AsList();

            if (li.Count == 0) return new
            {
                Status = -1,
                Message = $@"Song with title: ""{title}"" not found"
            };

            return new
            {
                Status = 0,
                Data = li,
            };
        }


        //Quynh
        [HttpGet("get/user/{username}")]
        public async Task<ActionResult<object>> getCollection(string username)
        {
            var li = (await Program.Sql.QueryAsync<Playlist>("select * from Playlist p where p.Username = @Username ", new Playlist
            {
                Username = username
            })).AsList();

            if (li.Count == 0) return new
            {
                Status = -1,
                Message = $@"Playlist of ""{username}"" not found"
            };

            return new
            {
                Status = 0,
                Data = li
            };
        }


        //Quynh
        [HttpDelete("{id}")]
        public async Task<ActionResult<object>> deleteCollection(int id)
        {
            try
            {
                await Program.Sql.ExecuteAsync("delete from Playlist where Playlist.Id = @Id ", new Playlist
                {
                    Id = id
                });

                return new
                {
                    Status = 0,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    Status = -1,
                    message = ex.Message
                };
            }
        }
        //Quynh
        [HttpDelete("delete/song/{PlaylistId}/{SongId}")]
        public async Task<ActionResult<object>> deletePlaylistSong(int PlaylistId, int SongId)
        {
            try
            {
                await Program.Sql.ExecuteAsync("delete from PlaylistSong where PlaylistId = @PlaylistId and SongId = @SongId ", new
                {
                    PlaylistId = PlaylistId,
                    SongId = SongId
                });

                return new
                {
                    Status = 0,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    Status = -1,
                    message = ex.Message
                };
            }
        }
        //Quynh
        [HttpPost("update/ispublic/{check:bool}/{playlistId:int}")]
        public async Task<ActionResult<object>> updatePubicCollection(bool check, int playlistId)
        {
            try
            {
                await Program.Sql.ExecuteAsync("update Playlist set IsPublic = @IsPublic where Id=@Id", new Playlist
                {
                    Id = playlistId,
                    IsPublic = check
                });

                return new
                {
                    Status = 0,
                };

            }
            catch (Exception ex)
            {
                return new
                {
                    Status = -1,
                    message = ex.Message
                };
            }
        }

        //Quynh
        [HttpPost("update")]
        public async Task<ActionResult<object>> updatePlaylistCollection(JsonElement json)
        {
            try
            {
                Playlist playlist = json.GetProperty("playlist").GetObject<Playlist>(true);
                List<SongChoosen> list = json.GetProperty("list").GetObject<List<SongChoosen>>(true);


                await Program.Sql.ExecuteAsync("update Playlist set Thumbnail=@Thumbnail, Title=@Title, Description=@Description where Id=@Id", playlist);
                await Program.Sql.ExecuteAsync("delete from PlaylistSong where PlaylistId =@Id", playlist);
                foreach (SongChoosen song in list)
                {
                    if (song.Choosen)
                    {
                        await Program.Sql.ExecuteAsync("insert into PlaylistSong(PlaylistId, SongId) values(@PlaylistId,@SongId)", new
                        {
                            PlaylistId = playlist.Id,
                            SongId = song.Song.Id
                        });
                    }
                }
                return new
                {
                    Status = 0,
                };

            }
            catch (Exception ex)
            {
                return new
                {
                    status = -1,
                    message = ex.Message
                };
            }

        }
        //Quynh
        [HttpPost("insert")]
        public async Task<ActionResult<object>> insertPlaylist(JsonElement json)
        {
            try
            {
                Playlist playlist = json.GetProperty("playlist").GetObject<Playlist>(true);
                List<SongChoosen> list = json.GetProperty("list").GetObject<List<SongChoosen>>(true);
                var username = json.GetProperty("username").GetObject<String>(true);

                await Program.Sql.ExecuteAsync("insert into Playlist(Username, Thumbnail, Title, Description, IsPublic) Values(@username,@thumbnail,@title,@description, @isPublic)", new
                {
                    username = username,
                    thumbnail = playlist.Thumbnail,
                    title = playlist.Title,
                    description = playlist.Description,
                    isPublic = playlist.IsPublic
                });

                var PId = (await Program.Sql.QueryAsync<int>("select TOP(1) p.Id from Playlist p where p.Username = @Username order by p.Id desc", new
                {
                    Username = username
                }));

                foreach (SongChoosen song in list)
                {
                    if (song.Choosen)
                    {
                        await Program.Sql.ExecuteAsync("insert into PlaylistSong(PlaylistId, SongId) values(@PlaylistId,@SongId)", new
                        {
                            PlaylistId = PId,
                            SongId = song.Song.Id
                        });
                    }
                }
                return new
                {
                    Status = 0,
                };

            }
            catch (Exception ex)
            {
                return new
                {
                    status = -1,
                    message = ex.Message
                };
            }

        }

        //Quynh
        [HttpGet("get/listchoosen/{username}/{Id}/{type}")]
        public async Task<ActionResult<object>> getListChoosen(string username, int Id, int type)
        {
            //type = 1: for new playlist
            //type = 2: for edit playlist

            var listSong = (await Program.Sql.QueryAsync<Song>("select * from Song s where s.Username = @Username", new
            {
                Username = username
            })).AsList();

            var listSongChoosen = (await Program.Sql.QueryAsync<int>("select SongId from PlaylistSong s where s.PlaylistId = @Id", new
            {
                Id = Id
            })).AsList();

            var li = new List<SongChoosen>();

            foreach (var song in listSong)
            {
                SongChoosen songChoosen = new SongChoosen();
                songChoosen.Song = song;
                if (type == 2)
                {
                    foreach (var Choosen in listSongChoosen)
                    {
                        if (song.Id == Choosen)
                        {
                            songChoosen.Choosen = true;
                            break;
                        }
                    }
                }
                li.Add(songChoosen);
            }

            return new
            {
                Status = 0,
                Data = li
            };
        }

        // Canh
        [HttpGet("get/songs/{id:int}")]
        public async Task<ActionResult<object>> getSongs(int id)
        {
            try
            {
                string sql = "SELECT * FROM Song s, "
                     + "(SELECT SongId FROM PlaylistSong WHERE PlaylistId = @Id) id "
                     + "WHERE s.Id = id.SongId AND s.Status != 1";

                var li = (await Program.Sql.QueryAsync<Song>(sql, new
                {
                    @Id = id
                })).AsList();

                if (li.Count == 0) return new
                {
                    Status = -1,
                    Message = $@"Playlist do not has any song!"
                };

                return new
                {
                    Status = 0,
                    Data = li
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    Status = -1,
                    Message = ex.Message
                };
            };
        }

        // Nguyen Khanh Duy
        [HttpPost("insert/song")]
        public async Task<ActionResult<object>> insertSong(JsonElement json)
        {
            try
            {
                Playlist playlist = json.GetProperty("playlist").GetObject<Playlist>(true);
                Song song = json.GetProperty("song").GetObject<Song>(true);
                await Program.Sql.ExecuteAsync("insert into PlaylistSong(PlaylistId, SongId) values(@PlaylistId, @SongId)", new
                {
                    PlaylistId = playlist.Id,
                    SongId = song.Id
                });

                return new
                {
                    Status = 0
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    Status = -1,
                    Message = ex.Message
                };
            };
        }
    }

}