﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using MUZIC.Model;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;

namespace MUZIC.API.Controllers
{
    public class UserController : ConBase
    {
        [HttpPost("create")]
        public async Task<ActionResult<object>> create([FromForm] string username, [FromForm] string password, [FromForm] string email)
        {
            try
            {
                await Program.Sql.ExecuteAsync("insert into Account(Username, Password, Email, DisplayName) values(@Username, @Password, @Email, @Username)", new
                {
                    Username = username,
                    Password = getHash(password),
                    Email = email
                });

                return new
                {
                    Status = 0
                };

            }
            catch (Exception)
            {
                return new
                {
                    Status = -1,
                    Message = $@"Username ""{username}"" exists"
                };
            }
        }

        [HttpGet("get/{username}")]
        public async Task<ActionResult<object>> get(string username)
        {
            var li = (await Program.Sql.QueryAsync<Account>("select * from Account  where Username=@Username", new Account
            {
                Username = username
            })).AsList();

            if (li.Count == 0) return new
            {
                Status = -1,
                Message = $@"Username ""{username}"" not found"
            };

            return new
            {
                Status = 0,
                Data = li.First()
            };
        }


        [HttpPost("login")]
        public async Task<ActionResult<object>> login([FromForm] string username, [FromForm] string password)
        {
            password = getHash(password);
            var li = (await Program.Sql.QueryAsync<Account>("select * from Account  where Username=@Username and Password=@Password", new
            {
                Username = username,
                Password = password
            })).AsList();

            if (li.Count == 0) return new
            {
                Status = -1,
                Message = $@"Username or password is incorrect!"
            };

            if (li.First().Status == 2) return new
            {
                Status = -1,
                Message = $@"Account '{li.First().Username}' is banned!"
            };

            return new
            {
                Status = 0
            };
        }

        public static string getHash(string s)
        {
            var data = s;
            var secretKey = Program.Config["SerectKey"];

            // Initialize the keyed hash object using the secret key as the key
            var hashObject = new HMACSHA256(Encoding.UTF8.GetBytes(secretKey));

            // Computes the signature by hashing the salt with the secret key as the key
            var signature = hashObject.ComputeHash(Encoding.UTF8.GetBytes(data));

            // Encode
            var encodedSignature = Base64UrlEncoder.Encode(signature);

            return encodedSignature;
        }

        //Quynh
        [HttpPost("avatar")]
        public async Task<ActionResult<object>> updateAvatar(JsonElement json)
        {
            try
            {
                var username = json.GetProperty("username").GetObject<String>(true);
                var img = json.GetProperty("img").GetObject<String>(true);
                await Program.Sql.ExecuteAsync("update Account set Avatar =@Avatar where Username = @Username", new
                {
                    Username = username,
                    Avatar = img
                });

                return new
                {
                    Status = 0,
                };

            }
            catch (Exception ex)
            {
                return new
                {
                    Status = -1,
                    message = ex.Message
                };
            }
        }

    }
}
