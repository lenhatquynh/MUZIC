﻿using Microsoft.AspNetCore.Mvc;

namespace MUZIC.API.Controllers
{
    public class WelcomeController : ConBase
    {
        [HttpGet]
        public ActionResult<object> Get()
        {
            return new
            {
                greeting = "Welcome to MUZIC App API Server",
                version = "1.0"
            };
        }

        [HttpGet("test")]
        public ActionResult<object> TestApi()
        {
            return new
            {
                greeting = "Welcome to MUZIC App API Test",
                version = "2.0"
            };
        }
    }
}
