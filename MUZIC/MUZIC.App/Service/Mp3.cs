﻿using Microsoft.JSInterop;
using MUZIC.Model;

namespace MUZIC.App.Service
{
    public class Mp3
    {
        private readonly IJSRuntime js;

        public Mp3(IJSRuntime _js) {
            js = _js;
        }

        public async Task AddSong(Song song)
        {
            await js.InvokeVoidAsync("add_song", song);
        }

        public async Task PlaySong(Song song)
        {
            await js.InvokeVoidAsync("play_song", song);
        }

        public async Task AddPlaylist(List<Song> playlist)
        {
            await js.InvokeVoidAsync("add_playlist", playlist);
        }

        public async Task PlayPlaylist(List<Song> playlist)
        {
            await js.InvokeVoidAsync("play_playlist", playlist);
        }
    }
}
