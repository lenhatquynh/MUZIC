﻿using MUZIC.Model;
using RestSharp;
using System.Text.Json;
using MUZIC.App.Utility.Extension;

namespace MUZIC.App.Service
{
    public class TrackingService
    {
        public static async Task AddSongViewCount(string username, int songId)
        {
            var request = new RestRequest("tracking/add/viewcount/song", Method.Post);
            request.AddParameter("username", username);
            request.AddParameter("songId", songId);

            var res = await Program.Client.ExecuteAsync(request);
            if (!res.IsSuccessful) Console.WriteLine("AddSongViewCount: error");
        }

        public static async Task AddPlaylistViewCount(string username, int playlistId)
        {
            var request = new RestRequest("tracking/add/viewcount/playlist", Method.Post);
            request.AddParameter("username", username);
            request.AddParameter("playlistId", playlistId);

            var res = await Program.Client.ExecuteAsync(request);
            if (!res.IsSuccessful) Console.WriteLine("AddPlaylistViewCount: error");
        }

        public static async Task<List<int>> GetSongViewDataYesterday(int songId)
        {
            var request = new RestRequest($"tracking/get/yesterday/view/song/{songId}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            if (!res.IsSuccessful) Console.WriteLine("GetSongViewDataYesterday: error");
            var json = JsonDocument.Parse(res.Content!).RootElement;
            return json.GetProperty("data").GetObject<List<int>>();
        }

        public static async Task<List<int>> GetSongFavoriteDataYesterday(int songId)
        {
            var request = new RestRequest($"tracking/get/yesterday/favorite/song/{songId}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            if (!res.IsSuccessful) Console.WriteLine("GetSongViewDataYesterday: error");
            var json = JsonDocument.Parse(res.Content!).RootElement;
            return json.GetProperty("data").GetObject<List<int>>();
        }

        // Canh
        public static async Task<int> GetSongViewCount(int songId)
        {
            var request = new RestRequest($"/tracking/get/viewcount/song/{songId}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;

            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<int>();
                return r;
            }
            return 0;
        }

        // Canh
        public static async Task<int> GetPlaylistViewCount(int playlistId)
        {
            var request = new RestRequest($"/tracking/get/viewcount/playlist/{playlistId}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;

            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<int>();
                return r;
            }
            return 0;
        }

        //Quynh
        public static async Task<List<TopSong>> GetTopViewSong(string day, int top)
        {
            var request = new RestRequest($"/tracking/get/topview/song/{day}/{top}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;

            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<TopSong>>();
                return r;
            }
            return new List<TopSong>();
        }

        //Quynh
        public static async Task<List<TopSong>> GetTopFavoriteSong(string day, int top)
        {
            var request = new RestRequest($"/tracking/get/topfavorite/song/{day}/{top}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;

            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<TopSong>>();
                return r;
            }
            return new List<TopSong>();
        }

        //Quynh
        public static async Task<List<TopPlaylist>> GetTopViewPlaylist(string day, int top)
        {
            var request = new RestRequest($"/tracking/get/topview/playlist/{day}/{top}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;

            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<TopPlaylist>>();
                return r;
            }
            return new List<TopPlaylist>();
        }

        //Quynh
        public static async Task<List<Playlist>> GetTopFavoritePlaylist(string day, int top)
        {
            var request = new RestRequest($"/tracking/get/topfavorite/playlist/{day}/{top}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;

            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<Playlist>>();
                return r;
            }
            return new List<Playlist>();
        }

        // Quynh
        public static async Task<int> GetSongFavoriteCount(int songId)
        {
            var request = new RestRequest($"/tracking/get/favoritecount/song/{songId}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;

            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<int>();
                return r;
            }
            return 0;
        }

        // Quynh
        public static async Task<int> GetPlaylistFavoriteCount(int playlistId)
        {
            var request = new RestRequest($"/tracking/get/favoritecount/playlist/{playlistId}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;

            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<int>();
                return r;
            }
            return 0;
        }


        //Quynh
        public static async Task<List<TopSong>> GetTopViewSongAllTime(int top)
        {
            var request = new RestRequest($"/tracking/get/topview/all/song/{top}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;

            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<TopSong>>();
                return r;
            }
            return new List<TopSong>();
        }


        //Quynh
        public static async Task<List<TopSong>> GetTopFavoriteSongAllTime(int top)
        {
            var request = new RestRequest($"/tracking/get/topfavorite/all/song/{top}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;

            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<TopSong>>();
                return r;
            }
            return new List<TopSong>();
        }

        //Quynh
        public static async Task<List<TopPlaylist>> GetTopViewPlaylistAllTime(int top)
        {
            var request = new RestRequest($"/tracking/get/topview/all/playlist/{top}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;

            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<TopPlaylist>>();
                return r;
            }
            return new List<TopPlaylist>();
        }
        //Quynh
        public static async Task<List<TopPlaylist>> GetTopFavoritePlaylistAllTime(int top)
        {
            var request = new RestRequest($"/tracking/get/topfavorite/all/playlist/{top}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;

            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<TopPlaylist>>();
                return r;
            }
            return new List<TopPlaylist>();
        }
    }
}
