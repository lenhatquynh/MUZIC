﻿using Imagekit;
using Microsoft.AspNetCore.Components.Forms;

namespace MUZIC.App.Utility
{
    public class SongMetadata
    {
        public string? Title { get; set; }
        public string? Artist { get; set; }
        public string? Genre { get; set; }
        public int Duration { get; set; }
    }

    public static class Util
    {
        public static async Task<SongMetadata> GetSongMetadata(IBrowserFile file)
        {
            string filePath = $@"{Program.RootPath}Data\{DateTime.Now.Ticks}.{file.Name}";
            using var orgStream = file.OpenReadStream(25000000);

            using (var fileStream = File.Create(filePath))
            {
                await orgStream.CopyToAsync(fileStream);
            }

            using var tf = TagLib.File.Create(filePath);
            var res = new SongMetadata
            {
                Title = tf.Tag.Title,
                Artist = tf.Tag.FirstPerformer ?? tf.Tag.FirstAlbumArtist ?? tf.Tag.FirstComposer,
                Genre = tf.Tag.FirstGenre,
                Duration = (int)tf.Properties.Duration.TotalSeconds
            };

            File.Delete(filePath);
            return res;
        }

        public static async Task<ImagekitResponse> UploadFile(string path, IBrowserFile file)
        {
            using var orgStream = file.OpenReadStream(25000000);

            var fileBytes = await ToBytes(orgStream);
            var result = await Program.Imagekit.Folder(path).FileName(file.Name).UploadAsync(fileBytes);
            return result;
        }

        public static string GetImage(string url, Transformation? transform)
        {
            if (transform is null || !url.StartsWith("https://ik.imagekit.io")) return url;
            return Program.Imagekit.Url(transform).Src(url).Generate();
        }

        public static async Task<byte[]> ToBytes(Stream instream)
        {
            if (instream is MemoryStream)
                return ((MemoryStream)instream).ToArray();

            using (var memoryStream = new MemoryStream())
            {
                await instream.CopyToAsync(memoryStream);
                return memoryStream.ToArray();
            }
        }

        public static string TimeFormat(int secs)
        {
            string answer;
            TimeSpan t = TimeSpan.FromSeconds(secs);

            if (t.Hours != 0)
            {
                answer = string.Format("{0:D2}:{1:D2}:{2:D2}",
                           t.Hours,
                           t.Minutes,
                           t.Seconds);
            }
            else
            {
                answer = string.Format("{0:D2}:{1:D2}",
                           t.Minutes,
                           t.Seconds);
            }
            return answer;
        }
    }
}
