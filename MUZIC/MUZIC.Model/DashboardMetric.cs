﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MUZIC.Model
{
    public class DashboardMetric
    {
        public int cnt_feedback { get; set; }
        public int cnt_account {get; set;}
        public int cnt_song {get; set;}
        public int cnt_playlist {get; set;}
        public int today_web_visited {get; set;}
        public int today_song_view {get; set;}
        public int today_song_created {get; set;}
        public int today_playlist_created {get; set;}
        public int today_account_created {get; set;}
        public int today_feedback_view {get; set;}
        public int total_song_view {get; set;}
        public int total_playlist_view {get; set;}
        public int total_favorite {get; set;}
        public int total_genres {get; set;}

    }
}
