﻿namespace MUZIC.Model
{
    public class Feedback
    {
        public int Id { get; set; }
        public string Username { get; set; } = null!;
        public string Title { get; set; } = null!;
        public DateTime Date { get; set; }
        public int Priority { get; set; }
        public string Detail { get; set; } = null!;
    }
}