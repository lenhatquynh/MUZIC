﻿namespace MUZIC.Model
{
    public class WebTracking
    {

        public string? Username { get; set; }
        public string ContextURL { get; set; } = null!;
        public DateTime Date { get; set; }
    }
}