﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MUZIC.Test;

[TestFixture]
public class SongTest
{
    [SetUp]
    public void Setup()
    {
        MUZIC.App.TestHelper.Enable();
    }

    [TestCaseSource(typeof(Util), "GetData", new object[] { "song/getSong.json" })]
    public async Task GetSong(JsonElement[] p)
    {
        Assert.Ignore();

        int id = p[0].GetInt32();
        int expectedId = p[1].GetInt32();
        // get song
        Song? song = await SongService.GetSong(id);

        if (expectedId == -1)
        {
            Assert.IsNull(song);
        }
        else
        {
            Assert.AreEqual(expectedId, song?.Id);
        }
    }

    [TestCaseSource(typeof(Util), "GetData", new object[] { "song/getCollection.json" })]
    public async Task GetCollection(JsonElement[] p)
    {
        Assert.Ignore();

        string username = p[0].ToString();
        bool expectedResult = p[1].GetBoolean();
        // get song
        List<Song>? songs = await SongService.GetCollection(username);

        if (expectedResult)
        {
            foreach (var song in songs!)
            {
                Assert.AreEqual(song.Username, username);
            }
        }
        else
        {
            Assert.IsNull(songs);
        }
    }

    [TestCaseSource(typeof(Util), "GetData", new object[] { "song/deleteSong.json" })]
    public void DeleteSong(JsonElement[] p)
    {
        Assert.Ignore();

        int id = p[0].GetInt32();

        Assert.DoesNotThrowAsync(async () =>
        {
            // delete song
            await SongService.DeleteCollection(id);
        });

    }

    [TestCaseSource(typeof(Util), "GetData", new object[] { "song/updatePublicSong.json" })]
    public void UpdatePublicSong(JsonElement[] p)
    {
        Assert.Ignore();

        bool isPublic = p[0].GetBoolean();
        int id = p[1].GetInt32();

        // update public song
        Assert.DoesNotThrowAsync(async () =>
        {
            await SongService.UpdatePubicCollection(isPublic, id);
        });

    }

    [TestCaseSource(typeof(Util), "GetData", new object[] { "song/getTop.json" })]
    public async Task GetTop(JsonElement[] p)
    {
        Assert.Ignore();

        int n = p[0].GetInt32();
        int expectedResult = p[1].GetInt32();

        // get top songs
        List<Song>? songs = await SongService.GetTop(n);

        Assert.LessOrEqual(expectedResult, songs?.Count);
    }

}
