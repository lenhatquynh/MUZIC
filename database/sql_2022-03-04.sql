USE [master]
GO
/****** Object:  Database [MUZIC]    Script Date: 3/4/2022 6:45:54 PM ******/
CREATE DATABASE [MUZIC]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MUZIC', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\MUZIC.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'MUZIC_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\MUZIC_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [MUZIC] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MUZIC].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MUZIC] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MUZIC] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MUZIC] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MUZIC] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MUZIC] SET ARITHABORT OFF 
GO
ALTER DATABASE [MUZIC] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MUZIC] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MUZIC] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MUZIC] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MUZIC] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MUZIC] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MUZIC] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MUZIC] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MUZIC] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MUZIC] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MUZIC] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MUZIC] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MUZIC] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MUZIC] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MUZIC] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MUZIC] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MUZIC] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MUZIC] SET RECOVERY FULL 
GO
ALTER DATABASE [MUZIC] SET  MULTI_USER 
GO
ALTER DATABASE [MUZIC] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MUZIC] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MUZIC] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MUZIC] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [MUZIC] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [MUZIC] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'MUZIC', N'ON'
GO
ALTER DATABASE [MUZIC] SET QUERY_STORE = OFF
GO
USE [MUZIC]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 3/4/2022 6:45:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[Username] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](max) NULL,
	[IsAdmin] [bit] NULL,
	[Date] [datetime] NULL,
	[Status] [int] NULL,
	[Avatar] [nvarchar](max) NULL,
	[DisplayName] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 3/4/2022 6:45:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedback](
	[Username] [nvarchar](100) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Detail] [nvarchar](max) NOT NULL,
	[Priority] [int] NOT NULL,
	[Date] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Playlist]    Script Date: 3/4/2022 6:45:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Playlist](
	[PlaylistId] [int] NOT NULL,
	[Username] [nvarchar](100) NOT NULL,
	[Thumbnail] [nvarchar](max) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Date] [datetime] NOT NULL,
	[IsPublic] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PlaylistSong]    Script Date: 3/4/2022 6:45:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlaylistSong](
	[PlaylistId] [int] NOT NULL,
	[SongId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PlaylistTracking]    Script Date: 3/4/2022 6:45:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlaylistTracking](
	[UserId] [int] NOT NULL,
	[PlaylistId] [int] NOT NULL,
	[EventType] [int] NOT NULL,
	[Date] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Song]    Script Date: 3/4/2022 6:45:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Song](
	[SongId] [int] NOT NULL,
	[Username] [nvarchar](100) NOT NULL,
	[Src] [nvarchar](max) NOT NULL,
	[Thumbnail] [nvarchar](max) NOT NULL,
	[Cover] [nvarchar](max) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Artist] [nvarchar](max) NOT NULL,
	[Genre] [nvarchar](max) NOT NULL,
	[Lyrics] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Duration] [int] NOT NULL,
	[IsPublic] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SongTracking]    Script Date: 3/4/2022 6:45:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SongTracking](
	[UserId] [int] NOT NULL,
	[SongId] [int] NOT NULL,
	[EventType] [nvarchar](max) NOT NULL,
	[Date] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'duy', N'123', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'duydn', N'123', 0, CAST(N'2022-02-26T15:50:59.090' AS DateTime), 0, N'/img/defaultAvatar.jpg', NULL, NULL)
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'nhatquynh', N'50-8Q9jsl92Yu7ZvOcNMFvL0wq-79rYE4IUeGeD9qJk', 0, CAST(N'2022-03-01T21:21:11.437' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'nhatquynh', N'nhatquynh24092001@gmail.com')
GO
INSERT [dbo].[Playlist] ([PlaylistId], [Username], [Thumbnail], [Title], [Description], [Date], [IsPublic]) VALUES (1, N'minhcanh', N'https://www.google.com/url?sa=i&url=https%3A%2F%2Fmaychuvietnam.com.vn%2Floi-bai-hat-am-tham-ben-em-ca-si-son-tung-mtp%2F&psig=AOvVaw2Y0mGY_XO8R_vmTgUFrcDV&ust=1646225441200000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCOCk6ef5pPYCFQAAAAAdAAAAABAO', N'đây là list nhạc của sơn tùng mtp', N'Enjoy your time', CAST(N'2022-03-01T15:30:00.000' AS DateTime), 1)
INSERT [dbo].[Playlist] ([PlaylistId], [Username], [Thumbnail], [Title], [Description], [Date], [IsPublic]) VALUES (2, N'nhatquynh', N'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D4CCGI83vOVo&psig=AOvVaw0Sg5aJZOsvWNU_GCq_lBoJ&ust=1646226016879000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCMDYqfv7pPYCFQAAAAAdAAAAABAD', N'đây là list nhạc của Jack', N'Enjoy your time', CAST(N'2022-03-01T15:30:00.000' AS DateTime), 1)
GO
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (1, 1)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (1, 2)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (1, 3)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (1, 4)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (1, 5)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (1, 6)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (2, 1)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (2, 2)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (2, 3)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (2, 4)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (2, 5)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (2, 6)
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT ((0)) FOR [IsAdmin]
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT ('/img/defaultAvatar.jpg') FOR [Avatar]
GO
USE [master]
GO
ALTER DATABASE [MUZIC] SET  READ_WRITE 
GO
